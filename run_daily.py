import schedule
import time
import lunch_checker

def job():
    lunch_checker.check_now()
    return

timeOfDay = "07:30"

schedule.every().day.at(timeOfDay).do(job)

while True:
    schedule.run_pending()
    time.sleep(60)
