import requests
import time
import datetime
from bs4 import BeautifulSoup
from email_handler import Class_eMail

# For live-kitchen.co.uk
username="some-username"
password="some-password"

# Who gets an email
toEmail="some-email@gmail.com"

def check_now():

    URL="https://app.live-kitchen.co.uk/login.aspx"
    headers={"User-Agent":"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36"}

    s=requests.Session()
    s.headers.update(headers)
    r=s.get(URL)
    soup=BeautifulSoup(r.content, "lxml")

    VIEWSTATE=soup.find(id="__VIEWSTATE")['value']
    VIEWSTATEGENERATOR=soup.find(id="__VIEWSTATEGENERATOR")['value']
    EVENTVALIDATION=soup.find(id="__EVENTVALIDATION")['value']

    login_data={"__VIEWSTATE":VIEWSTATE,
    "__VIEWSTATEGENERATOR":VIEWSTATEGENERATOR,
    "__EVENTVALIDATION":EVENTVALIDATION,
    "ctl00$bodyContent$username":username,
    "ctl00$bodyContent$password":password,
    "ctl00$bodyContent$btnLogin":"Sign In"}

    time.sleep(.5)

    print "Asking nicely"

    r = s.post(URL, data=login_data)

    time.sleep(.5)

    r = s.get("https://app.live-kitchen.co.uk/parent_list_orders.aspx")

    soup = BeautifulSoup(r.content, "lxml")

    date = soup.find(id="bodyContent_lvwOrders_lblMealDate_0").contents[0]
    order = soup.find(id="bodyContent_lvwOrders_lblMainCourseTitle_0").contents[0]
    order2 = soup.find(id="bodyContent_lvwOrders_lblMainCourseTitle_1").contents[0]

    print "Order on " + date + " is " + order

    today = datetime.datetime.today().strftime("%a %d %b")
    if date != today:
        print "That's not today, phew."
        exit()

    email = Class_eMail()
    message = order + "\n\n(Then it's " + order2 + ")"
    email.send_Text_Mail(toEmail, "Lunch on " + date, message)
    del email
