## School Lunch Checker

Emails you today's lunch order, so you have time to change it.

Works with www.live-kitchen.co.uk and any SMTP email server.

## Dependencies

```sh
apt install python-pip
pip install -r requirements.txt
apt-get install python-lxml
```

## Run on startup

```sh
sudo nano /etc/rc.local
```

Add to bottom:
```sh
sudo python -u /home/pi/lunch-checker/run_daily.py >> /home/pi/lunch-checker/log.txt 2>&1 &
```

## Reboot every night

Avoids crashes.
```sh
sudo crontab -e
```

Add to bottom:
```sh
@midnight /sbin/shutdown -r now
```

## Troubleshooting

Look at `log.txt`.
